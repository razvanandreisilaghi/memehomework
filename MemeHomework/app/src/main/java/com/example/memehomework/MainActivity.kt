package com.example.memehomework

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)


        val retrofit: Retrofit = Retrofit.Builder()
                                    .baseUrl("https://api.imgflip.com/")
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build()

        val apiService: ApiService = retrofit.create(ApiService::class.java)


        val call: Call<Data> = apiService.getMemes()


        call.enqueue(object: Callback<Data> {
            override fun onResponse(call: Call<Data>, response: Response<Data>) {
                if(!response.isSuccessful) {
                    Toast.makeText(this@MainActivity, response.code(), Toast.LENGTH_LONG).show()
                    return
                }
                val dataBody: Data? = response.body()
                val memeList: List<Meme>? = dataBody?.data?.memes
                val adapter: RecyclerViewAdapter = RecyclerViewAdapter(memeList as MutableList<Meme>, this@MainActivity)
                recyclerView.adapter = adapter
            }

            override fun onFailure(call: Call<Data>, t: Throwable) {
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_LONG).show()
            }


        })
    }
}