package com.example.memehomework

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.app.ShareCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_meme.view.*

class RecyclerViewAdapter(
    private val memes: MutableList<Meme>,
    private val context: Context
): RecyclerView.Adapter<RecyclerViewAdapter.MemeViewHolder>() {

    class MemeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemeViewHolder {
        return MemeViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_meme,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MemeViewHolder, position: Int) {
        val curentMeme = memes[position]

        Glide.with(context)
            .asBitmap()
            .load(curentMeme.url)
            .into(holder.itemView.urlImage)

        holder.itemView.nameText.text = curentMeme.name
        holder.itemView.resolutionText.text = curentMeme.width.toString() + " X " + curentMeme.height.toString()

        val thumbsUp: ImageView = holder.itemView.thumbsUp
        val thumbsDown: ImageView = holder.itemView.thumbsDown
        val share: ImageView = holder.itemView.shareButton
        thumbsUp.setOnClickListener{
            if(thumbsUp.colorFilter == null) {
                thumbsUp.setColorFilter(ContextCompat.getColor(context, R.color.green))
                thumbsDown.clearColorFilter()
            }
            else
            {
                thumbsUp.clearColorFilter()
            }
        }
        thumbsDown.setOnClickListener{
            if(thumbsDown.colorFilter == null) {
                thumbsDown.setColorFilter(ContextCompat.getColor(context, R.color.red))
                thumbsUp.clearColorFilter()
            }
            else
            {
                thumbsDown.clearColorFilter()
            }
        }
        share.setOnClickListener{
            ShareCompat.IntentBuilder.from(context as Activity)
                .setType("text/plain")
                .setChooserTitle("Sharing meme")
                .setText(curentMeme.url)
                .startChooser()
        }
    }

    override fun getItemCount(): Int {
        return memes.size
    }

}