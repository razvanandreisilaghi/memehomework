package com.example.memehomework

import com.google.gson.annotations.SerializedName

data class Data (

    @SerializedName("data")
    val data: MemeList

        )